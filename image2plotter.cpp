#include "image2plotter.hpp"
ImageToPlotter::ImageToPlotter(double max_vel_, double max_vel2_, double feed_speed_, int step_)
{
    max_vel = max_vel_;
    max_vel2 = max_vel2_;
    feed_speed = feed_speed_;
    step = step_;
}
void ImageToPlotter::makePlotterRoute(std::vector<RouteMaker::PointfArray> routes)
{
    long delay = (long)(1000*1000*feed_speed/max_vel/step/2);
    long delay2 = (long)(1000*1000*feed_speed/max_vel2/step/2);
    PlotterRoute tmp;
    float x,y;
    tmp.pen_down=0;
    if((x=routes[0][0].x)>0){
	tmp.x.cwccw = 0;
	tmp.x.pulse_num = (int)(x*step/feed_speed);
    }else{
	tmp.x.cwccw = 1;
	tmp.x.pulse_num = (int)(-x*step/feed_speed);
    }
    if((y=routes[0][0].y)>0){
	tmp.y.cwccw = 0;
	tmp.y.pulse_num = (int)(y*step/feed_speed);
    }else{
	tmp.y.cwccw = 1;
	tmp.y.pulse_num = (int)(-y*step/feed_speed);
    }
    if(tmp.x.pulse_num > tmp.y.pulse_num){
	tmp.x.delay = delay2;
	if(tmp.y.pulse_num != 0){
	    tmp.y.delay = delay2*tmp.x.pulse_num/tmp.y.pulse_num;
	}else{
	    tmp.y.delay = 0;
	}
    }else{
	tmp.y.delay = delay2;
	if(tmp.x.pulse_num != 0){
	    tmp.x.delay = delay2*tmp.y.pulse_num/tmp.x.pulse_num;
	}else{
	    tmp.x.delay = 0;
	}
    }
    plotter_routes.push_back(tmp);
    for(int i=0; i<routes.size(); i++){
	for(int j=0; j<routes[i].size()-1; j++){
	    tmp.pen_down = 1;
	    if((x=routes[i][j+1].x-routes[i][j].x)>0){
		tmp.x.cwccw = 0;
		tmp.x.pulse_num = (int)(x*step/feed_speed);
	    }else{
		tmp.x.cwccw = 1;
		tmp.x.pulse_num = (int)(-x*step/feed_speed);
	    }
	    if((y=routes[i][j+1].y-routes[i][j].y)>0){
		tmp.y.cwccw = 0;
		tmp.y.pulse_num = (int)(y*step/feed_speed);
	    }else{
		tmp.y.cwccw = 1;
		tmp.y.pulse_num = (int)(-y*step/feed_speed);
	    }
	    if(tmp.x.pulse_num > tmp.y.pulse_num){
		tmp.x.delay = delay;
		if(tmp.y.pulse_num != 0){
		    tmp.y.delay = delay*tmp.x.pulse_num/tmp.y.pulse_num;
		}else{
		    tmp.y.delay = 0;
		}
	    }else{
		tmp.y.delay = delay;
		if(tmp.x.pulse_num != 0){
		    tmp.x.delay = delay*tmp.y.pulse_num/tmp.x.pulse_num;
		}else{
		    tmp.x.delay = 0;
		}
	    }
	    plotter_routes.push_back(tmp);
	}
	if(i<routes.size()-1){
	    if(norm(routes[i+1].front()-routes[i].back())<1.0){
		routes[i+1].front() = routes[i].back();
		continue;
	    }
	    tmp.pen_down = 0;
	    if((x=routes[i+1].front().x-routes[i].back().x)>0){
		tmp.x.cwccw = 0;
		tmp.x.pulse_num = (int)(x*step/feed_speed);
	    }else{
		tmp.x.cwccw = 1;
		tmp.x.pulse_num = (int)(-x*step/feed_speed);
	    }
	    if((y=routes[i+1].front().y-routes[i].back().y)>0){
		tmp.y.cwccw = 0;
		tmp.y.pulse_num = (int)(y*step/feed_speed);
	    }else{
		tmp.y.cwccw = 1;
		tmp.y.pulse_num = (int)(-y*step/feed_speed);
	    }
	    if(tmp.x.pulse_num > tmp.y.pulse_num){
		tmp.x.delay = delay;
		if(tmp.y.pulse_num != 0){
		    tmp.y.delay = delay*tmp.x.pulse_num/tmp.y.pulse_num;
		}else{
		    tmp.y.delay = 0;
		}
	    }else{
		tmp.y.delay = delay;
		if(tmp.x.pulse_num != 0){
		    tmp.x.delay = delay*tmp.y.pulse_num/tmp.x.pulse_num;
		}else{
		    tmp.x.delay = 0;
		}
	    }
	    plotter_routes.push_back(tmp);
	}
    }
    tmp.pen_down=0;
    if((x=-routes.back().back().x)>0){
	tmp.x.cwccw = 0;
	tmp.x.pulse_num = (int)(x*step/feed_speed);
    }else{
	tmp.x.cwccw = 1;
	tmp.x.pulse_num = (int)(-x*step/feed_speed);
    }
    if((y=-routes.back().back().y)>0){
	tmp.y.cwccw = 0;
	tmp.y.pulse_num = (int)(y*step/feed_speed);
    }else{
	tmp.y.cwccw = 1;
	tmp.y.pulse_num = (int)(-y*step/feed_speed);
    }
    if(tmp.x.pulse_num > tmp.y.pulse_num){
	tmp.x.delay = delay2;
	if(tmp.y.pulse_num != 0){
	    tmp.y.delay = delay2*tmp.x.pulse_num/tmp.y.pulse_num;
	}else{
	    tmp.y.delay = 0;
	}
    }else{
	tmp.y.delay = delay2;
	if(tmp.x.pulse_num != 0){
	    tmp.x.delay = delay2*tmp.y.pulse_num/tmp.x.pulse_num;
	}else{
	    tmp.x.delay = 0;
	}
    }
    plotter_routes.push_back(tmp);
}
std::vector<PlotterRoute> ImageToPlotter::getPlotterRoutes()
{
    return plotter_routes;
}
