#include "route_maker.hpp"
#include "image2plotter.hpp"
#include "device.hpp"
#include <boost/thread.hpp>
#include <boost/bind.hpp>

enum CMD{
    DRAW,
    EXIT,
    STOP,
    PEN,
};
sem_t user_sem, ctrl_sem, input_sem;

CMD user_cmd;
CMD input_cmd;
std::vector<PlotterRoute> pr;
int ctrl_status;
static void input(void)
{
    char c;
    while(1){
	std::cin.get(c);
	switch(c){
	case 'd':
	    input_cmd = DRAW;
	    sem_post(&input_sem);
	    break;
	case 'e':
	    input_cmd = EXIT;
	    sem_post(&input_sem);
	    break;
	case 's':
	    input_cmd = STOP;
	    sem_post(&input_sem);
	    break;
	case 'p':
	    input_cmd = PEN;
	    sem_post(&input_sem);
	    break;
	default:
	    break;
	}
    }
}
static void stop_action(Stepper &st_x, Stepper &st_y, Solenoid &sl, int &ctrl_status, sem_t &ctrl_sem)
{
    ctrl_status = 1;
    sem_post(&ctrl_sem);
    st_x.cmd=0;
    st_y.cmd=0;
    sl.cmd=0;
    sem_post(&(st_x.sem));
    sem_post(&(st_y.sem));
    sem_post(&(sl.sem));
    sem_wait(&(st_x.rt_sem));
    sem_wait(&(st_y.rt_sem));
    sem_wait(&(sl.rt_sem));
    ctrl_status = 0;
    sem_post(&ctrl_sem);
}
static void control(void)
{
    int x_fin,y_fin;
    int stop_flag;
    Stepper st_x(1,2);
    Stepper st_y(3,4);
    Solenoid sl(5);
    boost::thread th_x(boost::bind(&Stepper::thread, &st_x));
    boost::thread th_y(boost::bind(&Stepper::thread, &st_y));
    boost::thread th_sl(boost::bind(&Solenoid::thread, &sl));
    while(1){
	sem_wait(&user_sem);
	stop_flag = 0;
	switch(user_cmd){
	case DRAW:
	    ctrl_status = 1;
	    sem_post(&ctrl_sem);
	    std::cout << "draw start!!" << std::endl;
	    for(int i=0; i<pr.size(); i++){
		st_x.data=pr[i].x;
		st_y.data=pr[i].y;
		sl.down=pr[i].pen_down;
		st_x.cmd=1;
		st_y.cmd=1;
		sl.cmd=1;
		sem_post(&(sl.sem));
		sem_wait(&(sl.rt_sem));
		sem_post(&(st_x.sem));
		sem_post(&(st_y.sem));
		x_fin = 0;
		y_fin = 0;
		while(!x_fin || !y_fin){
		    if(!sem_trywait(&(st_x.rt_sem))){
			x_fin = 1;
		    }
		    if(!sem_trywait(&(st_y.rt_sem))){
			y_fin = 1;
		    }
		    if(!sem_trywait(&user_sem)){
			if(user_cmd==STOP){
			    stop_action(st_x, st_y, sl, ctrl_status, ctrl_sem);
			    stop_flag = 1;
			    break;
			}
		    }
		}
		if(stop_flag){
		    break;
		}
	    }
	    if(stop_flag){
		break;
	    }
	    std::cout << "draw complete!!" << std::endl;
	    
	    ctrl_status = 0;
	    sem_post(&ctrl_sem);
	    break;
	case PEN:
	    std::cout << "pen test start" << std::endl;
	    ctrl_status = 1;
	    sem_post(&ctrl_sem);
	    sl.down = 1;
	    sl.cmd = 1;
	    sem_post(&(sl.sem));
	    sem_wait(&(sl.rt_sem));
	    sem_wait(&(user_sem));
	    switch(user_cmd){
	    case STOP:
		stop_action(st_x, st_y, sl, ctrl_status, ctrl_sem);
		break;
	    }
	    break;
	case EXIT:
	    st_x.cmd=-1;
	    st_y.cmd=-1;
	    sl.cmd=-1;
	    sem_post(&(st_x.sem));
	    sem_post(&(st_y.sem));
	    sem_post(&(sl.sem));
	    th_x.join();
	    th_y.join();
	    th_sl.join();
	    return;
	    break;
	}
    }
}
int main(int argc, const char * argv[])
{
    int busy = 0;
    sem_init(&user_sem,0,0);
    sem_init(&ctrl_sem,0,0);
    sem_init(&input_sem,0,0);
    boost::thread th_ctrl(control);
    boost::thread th_input(input);
    RouteMaker rm(cv::imread(argv[1],0), atoi(argv[2]),120, 150, 2.0);
    rm.thinning();
    rm.featureExtract();
    rm.makeLine();
    rm.makeRoute();
    rm.boundingRect();
    rm.makeRealRoute();
    //rm.thinOut();
    rm.makeImage();
    rm.display();
    ImageToPlotter itp(3.0, 6.0);
    itp.makePlotterRoute(rm.getRealRoutes());
    pr = itp.getPlotterRoutes();
    while(1){
	if(!sem_trywait(&ctrl_sem)){
	    busy = ctrl_status;
	}
	if(!sem_trywait(&input_sem)){
            if(busy){
		if(input_cmd == STOP){
		    user_cmd = input_cmd;
		    sem_post(&user_sem);
		    std::cout << "stop" << std::endl;
		}else{
		    std::cout << "busy!" << std::endl;
		}
	    }else{
		if(input_cmd == EXIT){
		    user_cmd = input_cmd;
		    sem_post(&user_sem);
		    th_ctrl.join();
		    std::cout << "exit" << std::endl;
		    break;
		}else{
		    user_cmd = input_cmd;
		    sem_post(&user_sem);
		}
	    }
	}
    }
    return 0; 
}
