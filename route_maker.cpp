#include "route_maker.hpp"

const cv::Point RouteMaker::array4[4] = {cv::Point(1,0),
					     cv::Point(0,-1),
					     cv::Point(-1,0),
					     cv::Point(0,1)};
const cv::Point RouteMaker::array8[8] = {cv::Point(1,0),
					     cv::Point(1,-1),
					     cv::Point(0,-1),
					     cv::Point(-1,-1),
					     cv::Point(-1,0),
					     cv::Point(-1,1),
					     cv::Point(0,1),
					 cv::Point(1,1)};

cv::Vec3b RouteMaker::HSV2RGB(float h, float s, float v)
{
    float r = v;
    float g = v;
    float b = v;
    if (s > 0.0f) {
	h *= 6.0f;
	int i = (int) h;
	float f = h - (float) i;
	switch (i) {
        default:
        case 0:
            g *= 1 - s * (1 - f);
            b *= 1 - s;
            break;
        case 1:
            r *= 1 - s * f;
            b *= 1 - s;
            break;
        case 2:
            r *= 1 - s;
            b *= 1 - s * (1 - f);
            break;
        case 3:
            r *= 1 - s;
            g *= 1 - s * f;
            break;
        case 4:
            r *= 1 - s * (1 - f);
            g *= 1 - s;
            break;
        case 5:
            g *= 1 - s;
            b *= 1 - s * f;
            break;
	}
    }
    cv::Vec3b ret;
    ret[0] = (unsigned char)(r*255.0);
    ret[1] = (unsigned char)(g*255.0);
    ret[2] = (unsigned char)(b*255.0);
    return ret;
}

void RouteMaker::encode(cv::Mat data, int i, int j, int a[])
{
    for(int k=0;k<8;k++){
	a[k] = (data.at<unsigned char>(i+array8[k].y,j+array8[k].x)==0)?0:1;
    }
}

int RouteMaker::connect(int a[])
{
    int sum = 0;
    for(int i=0;i<8;i++){
	int j=(i==0)?7:i-1;
	if(a[i]!=0&&a[j]==0){
	    sum++;
	}
    }
    return sum;
}

RouteMaker::RouteMaker(cv::Mat data, int threshold, int w, int h, double inter)
{
    paper_w = w;
    paper_h = h;
    interval = inter;
    src = data;
    cv::threshold(src, src, threshold, 255, CV_THRESH_BINARY);
    src_inv = 255-src;
    feature_img = cv::Mat(src.rows,src.cols,CV_8UC3);
    line_img = cv::Mat(src.rows,src.cols,CV_8UC3);
    route_img = cv::Mat(src.rows,src.cols,CV_8UC3);
}
void RouteMaker::thinning()
{
    int r=0;
    cv::Mat src_tmp = src_inv.clone();
    cv::Mat dst = src_inv.clone();
    do{
	int a[8], b[8];
	r=0;
	for(int i=1;i<src_tmp.rows-1;i++){
	    for(int j=1;j<src_tmp.cols-1;j++){
		if(src_tmp.at<unsigned char>(i,j)==0){
		    continue;
		}
		encode(src_tmp,i,j,a);
		encode(dst,i,j,b);
		int sum = 0;
		for(int k=0;k<8;k++){
		    sum+=a[k];
		}
		if(sum==0){
		    dst.at<unsigned char>(i,j)=0;
		}else if(sum >= 3 && sum <= 5){
		    if (connect(a) == 1 && connect(b) == 1) { // 連結性の保存
			for (int k = 1; k < 5; k++) { // 上部3点と左横
			    if (b[k] == 0) {
				unsigned char c = a[k];
				a[k] = 0; // その点だけを黒反転しても
				if (connect(a) != 1) { // 連結性が保存されるか
				    a[k] = c;
				    break;
				}
				a[k] = c; // 元に戻す
			    }
			    dst.at<unsigned char>(i,j)=0;; // すべて連結性が保存されたならば黒反転
			}		   
		    }
		}
		
		if (dst.at<unsigned char>(i,j) == 0) {
		    r++; // 反転数
		}
	    }
	}
	src_tmp=dst.clone();
    }while(r>0);
    for(int i=0; i<dst.rows-2 ;i++){
	for(int j=0; j<dst.cols-2;j++){
	    if(dst.at<unsigned char>(i,j)!=0&&dst.at<unsigned char>(i+1,j)!=0&&dst.at<unsigned char>(i+2,j)!=0&&dst.at<unsigned char>(i,j+1)!=0&&dst.at<unsigned char>(i+1,j+1)!=0&&dst.at<unsigned char>(i+2,j+1)!=0){
		dst.at<unsigned char>(i+1,j)=0;
	    }
	    if(dst.at<unsigned char>(i,j)!=0&&dst.at<unsigned char>(i,j+1)!=0&&dst.at<unsigned char>(i,j+2)!=0&&dst.at<unsigned char>(i+1,j)!=0&&dst.at<unsigned char>(i+1,j+1)!=0&&dst.at<unsigned char>(i+1,j+2)!=0){
		dst.at<unsigned char>(i,j+1)=0;
	    }
	}
    }
    thin = dst.clone();
}

void RouteMaker::featureExtract()
{
    cv::Mat filtered = cv::Mat::zeros(thin.rows, thin.cols, CV_8UC1);
    for(int i=1; i<filtered.rows-1;i++){
	for(int j=1; j<filtered.cols-1;j++){
	    if(thin.at<unsigned char>(i,j)!=0){
		filtered.at<unsigned char>(i,j)=thin.at<unsigned char>(i+1,j)/255+thin.at<unsigned char>(i-1,j)/255+thin.at<unsigned char>(i,j+1)/255+thin.at<unsigned char>(i,j-1)/255;
	    }
	}
    }
    features = filtered;
}

void RouteMaker::makeLine()
{
    int make_line_flag;
    std::vector<Line> ret;
    cv::Mat img_ = cv::Mat::zeros(features.rows,features.cols,CV_8UC1);
    cv::Mat feature_ = features.clone();
    for(int i=0; i<feature_.rows; i++){
	for(int j=0;j<feature_.cols;j++){
	    if(feature_.at<unsigned char>(i,j)!=0){
		img_.at<unsigned char>(i,j)=1;
		if(feature_.at<unsigned char>(i,j)==2){
		    feature_.at<unsigned char>(i,j)=0;
		}
	    }
	}
    }
    while(1){
	make_line_flag = 0;
	for(int i=0; i<feature_.rows; i++){
	    for(int j=0;j<feature_.cols;j++){
		if(feature_.at<unsigned char>(i,j)!=0){
		    Line line;
		    line.start_fe = features.at<unsigned char>(i,j);
		    line.start_id = i*img_.cols+j;
		    line.line.push_back(cv::Point(j,i));
		    feature_.at<unsigned char>(i,j)--;
		    if(feature_.at<unsigned char>(i,j)==0){
			img_.at<unsigned char>(i,j)=0;
		    }
		    int tmp_x=j,tmp_y=i;
		    int end_flag=0;
		    while(1){
			for(int k=0;k<4;k++){
			    if(img_.at<unsigned char>(tmp_y+array4[k].y, tmp_x+array4[k].x)!=0){
				if(line.line.size()>1){
				    std::vector<cv::Point>::reverse_iterator it =  line.line.rbegin();
				    ++it;
				    if((*it).x == tmp_x+array4[k].x && (*it).y == tmp_y+array4[k].y){
					continue;
				    }
				}
				tmp_x += array4[k].x;
				tmp_y += array4[k].y;
				line.line.push_back(cv::Point(tmp_x,tmp_y));
				if(feature_.at<unsigned char>(tmp_y,tmp_x)!=0){
				    end_flag=1;
				    feature_.at<unsigned char>(tmp_y,tmp_x)--;
				}
				if(feature_.at<unsigned char>(tmp_y,tmp_x)==0){
				    img_.at<unsigned char>(tmp_y,tmp_x)=0;
				}
				break;
			    }
			}
			if(end_flag==1){
			    line.end_fe=features.at<unsigned char>(tmp_y,tmp_x);
			    line.end_id=tmp_y*img_.cols+tmp_x;
			    break;
			}
		    }
		    std::vector<cv::Point>::iterator pt1,pt2;
		    std::vector<cv::Point>::reverse_iterator pt1_,pt2_;
		    cv::Point2f tmp;
		    pt1 =  line.line.begin();
		    pt2 = (line.line.size()<10)?line.line.end():line.line.begin()+8;
		    tmp = (*pt2)-(*pt1);
		    line.start_vec = tmp*(1.0/norm(tmp));
		    pt1_ =  line.line.rbegin();
		    pt2_ = (line.line.size()<10)?line.line.rend():line.line.rbegin()+8;
		    tmp = (*pt2_)-(*pt1_);
		    line.end_vec = tmp*(1.0/norm(tmp));
		    ret.push_back(line);
		    make_line_flag = 1;
		}
	    }
	}
	if(make_line_flag == 0){
	    break;
	}
    }
    while(1){
	make_line_flag = 0;
	for(int i=0; i<img_.rows; i++){
	    for(int j=0; j<img_.cols; j++){
		if(img_.at<unsigned char>(i,j) == 1){
		    Line line;
		    line.start_fe = 2;
		    line.start_id = i*img_.cols+j;
		    line.line.push_back(cv::Point(j,i));
		    img_.at<unsigned char>(i,j)=0;
		    int tmp_x=j,tmp_y=i;
		    int end_flag=0;
		    while(1){
			end_flag = 1;
			for(int k=0;k<4;k++){
			    if(img_.at<unsigned char>(tmp_y+array4[k].y, tmp_x+array4[k].x)!=0){
				if(line.line.size()>1){
				    std::vector<cv::Point>::reverse_iterator it =  line.line.rbegin();
				    ++it;
				    if((*it).x == tmp_x+array4[k].x && (*it).y == tmp_y+array4[k].y){
					continue;
				    }
				}
				end_flag = 0;
				tmp_x += array4[k].x;
				tmp_y += array4[k].y;
				line.line.push_back(cv::Point(tmp_x,tmp_y));
				img_.at<unsigned char>(tmp_y,tmp_x)=0;
				break;
			    }
			}
			if(end_flag==1){
			    line.end_fe=2;
			    line.end_id=tmp_y*img_.cols+tmp_x;
			    break;
			}
		    }
		    
		    std::vector<cv::Point>::iterator pt1,pt2;
		    std::vector<cv::Point>::reverse_iterator pt1_,pt2_;
		    cv::Point2f tmp;
		    pt1 =  line.line.begin();
		    pt2 = (line.line.size()<10)?line.line.end():line.line.begin()+8;
		    tmp = (*pt2)-(*pt1);
		    line.start_vec = tmp*(1.0/norm(tmp));
		    pt1_ =  line.line.rbegin();
		    pt2_ = (line.line.size()<10)?line.line.rend():line.line.rbegin()+8;
		    tmp = (*pt2_)-(*pt1_);
		    line.end_vec = tmp*(1.0/norm(tmp));
		    ret.push_back(line);
		    make_line_flag = 1;
		}
	    }
	}
	if(make_line_flag == 0){
	    break;
	}
    }
    lines = ret;
}

void RouteMaker::makeRoute()
{
    std::vector<Line> lines_tmp = lines;
    std::vector<PointArray> ret;
    int make_route_flag;
    PointArray route;
    int last_id;
    cv::Point2f last_vec;
    /*while(1){
	make_route_flag = 0;
	for(int i=0;i<lines_tmp.size();i++){
	    if(lines_tmp[i].start_fe==1){
		route = lines_tmp[i].line;
		last_id = lines_tmp[i].end_id;
		last_vec = lines_tmp[i].end_vec;
		lines_tmp.erase(lines_tmp.begin()+i);
		make_route_flag=1;
		break;
	    }else if(lines_tmp[i].end_fe==1){
		std::reverse(lines_tmp[i].line.begin(),lines_tmp[i].line.end());
		route = lines_tmp[i].line;
		last_id = lines_tmp[i].start_id;
		last_vec = lines_tmp[i].start_vec;
		lines_tmp.erase(lines_tmp.begin()+i);
		make_route_flag=1;
		break;
	    }
	}
	if(make_route_flag){
	    while(1){
		int connect_flag=0;
		for(int i=0;i<lines_tmp.size();i++){
		    if(lines_tmp[i].start_id==last_id && last_vec.dot(lines_tmp[i].start_vec)<-0.5){
			route.pop_back();
			route.insert(route.end(),lines_tmp[i].line.begin(),lines_tmp[i].line.end());
			last_id = lines_tmp[i].end_id;
			last_vec = lines_tmp[i].end_vec;
			lines_tmp.erase(lines_tmp.begin()+i);
			connect_flag=1;
			break;
		    }else if(lines_tmp[i].end_id==last_id && last_vec.dot(lines_tmp[i].end_vec)<-0.5){
			std::reverse(lines_tmp[i].line.begin(),lines_tmp[i].line.end());
			route.pop_back();;
			route.insert(route.end(),lines_tmp[i].line.begin(),lines_tmp[i].line.end());
			last_id = lines_tmp[i].start_id;
			last_vec = lines_tmp[i].start_vec;
			lines_tmp.erase(lines_tmp.begin()+i);
			connect_flag=1;
			break;
		    }
		}
		if(!connect_flag){
		    break;
		}
	    }
	    ret.push_back(route);
	}else{
	    break;
	}
    }
    while(1){
	make_route_flag=0;
	if(lines_tmp.size()!=0){
	    route = lines_tmp[0].line;
	    last_id = lines_tmp[0].end_id;
	    last_vec = lines_tmp[0].end_vec;
	    lines_tmp.erase(lines_tmp.begin());
	    make_route_flag=1;
	}
	if(make_route_flag){
	    while(1){
		int connect_flag=0;
		for(int i=0;i<lines_tmp.size();i++){
		    if(lines_tmp[i].start_id==last_id && last_vec.dot(lines_tmp[i].start_vec)<-0.5){
			route.pop_back();
			route.insert(route.end(),lines_tmp[i].line.begin(),lines_tmp[i].line.end());
			last_id = lines_tmp[i].end_id;
			last_vec = lines_tmp[i].end_vec;
			lines_tmp.erase(lines_tmp.begin()+i);
			connect_flag=1;
			break;
		    }else if(lines_tmp[i].end_id==last_id && last_vec.dot(lines_tmp[i].end_vec)<-0.5){
			std::reverse(lines_tmp[i].line.begin(),lines_tmp[i].line.end());
			route.pop_back();;
			route.insert(route.end(),lines_tmp[i].line.begin(),lines_tmp[i].line.end());
			last_id = lines_tmp[i].start_id;
			last_vec = lines_tmp[i].start_vec;
			lines_tmp.erase(lines_tmp.begin()+i);
			connect_flag=1;
			break;
		    }
		}
		if(!connect_flag){
		    break;
		}
	    }
	    ret.push_back(route);
	}else{
	    break;
	}
	}*/
    for(int i=0;i<lines_tmp.size();i++){
	ret.push_back(lines_tmp[i].line);
	}
    std::vector<PointArray> ret_arranged;
    ret_arranged.push_back(ret.front());
    ret.erase(ret.begin());
    while(ret.size()!=0){
	float min_dst = norm(ret_arranged.back().back()-ret[0].front());
	int index = 0;
	int fb = 0;
	for(int i=0;i<ret.size();i++){
	    if(norm(ret_arranged.back().back()-ret[i].front())<min_dst){
		min_dst = norm(ret_arranged.back().back()-ret[i].front());
		index = i;
		fb=0;
	    }
	    if(norm(ret_arranged.back().back()-ret[i].back())<min_dst){
		min_dst = norm(ret_arranged.back().back()-ret[i].back());
		index = i;
		fb=1;
	    }
	}
	if(fb==0){
	    ret_arranged.push_back(ret[index]);
	    ret.erase(ret.begin()+index);
	}else{
	    std::reverse(ret[index].begin(),ret[index].end());
	    ret_arranged.push_back(ret[index]);
	    ret.erase(ret.begin()+index);
	}
    }
    routes =  ret_arranged;
}
void RouteMaker::makeRealRoute()
{
    double scale;
    if((double)paper_w/brect.width < (double)paper_h/brect.height){
	scale = (double)paper_w/brect.width;
    }else{
	scale = (double)paper_h/brect.height;
    }
    for(int i=0; i<routes.size(); i++){
	PointfArray tmp;
	for(int j=0; j<routes[i].size(); j++){
	    cv::Point tmp2 = routes[i][j]-brect.tl();
	    cv::Point2f tmp3;
	    tmp3.x = tmp2.x*scale;
	    tmp3.y = tmp2.y*scale;
	    tmp.push_back(tmp3);
	}
	real_routes.push_back(tmp);
    }
    image_real_scale = scale;
}
void RouteMaker::thinOut()
{
    int pixel = interval / image_real_scale;
    std::vector<PointfArray> tmp;
    for(int i=0; i<real_routes.size(); i++){
	PointfArray tmp2;
	for(int j=0; j<real_routes[i].size()-1; j=j+pixel){
	    tmp2.push_back(real_routes[i][j]);
	}
	tmp2.push_back(real_routes[i].back());
	tmp.push_back(tmp2);
    }
    real_routes = tmp;
}
void RouteMaker::makeImage()
{
    std::random_device rnd;     // 非決定的な乱数生成器でシード生成機を生成
    std::mt19937 mt(rnd()); //  メルセンヌツイスターの32ビット版、引数は初期シード
    std::uniform_real_distribution<> randi(0.0, 1.0);
    cv::cvtColor(thin, feature_img, CV_GRAY2RGB);
    for(int i=0;i<lines.size(); i++){
	float c = randi(mt);
	for(int j=0;j<lines[i].line.size();j++){
	    line_img.at<cv::Vec3b>(lines[i].line[j].y, lines[i].line[j].x) = HSV2RGB(c,1.0,1.0);
	}
    }
    for(int i=0;i<routes.size(); i++){
	float c = i*1.0/routes.size()/*randi(mt)*/;
	for(int j=0;j<routes[i].size();j++){
	    route_img.at<cv::Vec3b>(routes[i][j].y, routes[i][j].x) = HSV2RGB(c,1.0,1.0);
	}
	std::stringstream ss;
	ss << i;
	cv::Vec3b tmp=HSV2RGB(c,1.0,1.0);
	cv::putText(route_img, ss.str().c_str(), routes[i][0], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(tmp[0],tmp[1],tmp[2]), 1, CV_AA);
    }
    for(int i=0;i<features.rows;i++){
	for(int j=0;j<features.cols;j++){
	    switch((int)features.at<unsigned char>(i,j)){
	    case 1:
		cv::circle(feature_img, cv::Point(j, i), 5, cv::Scalar(0,200,0), 1, CV_AA);
		break;
	    case 3:
		cv::circle(feature_img, cv::Point(j, i), 5, cv::Scalar(0,0,200), 1, CV_AA);
		break;
	    case 4:
		cv::circle(feature_img, cv::Point(j, i), 5, cv::Scalar(0,200,200), 1, CV_AA);
		break;
	    }
	}
    }
    cv::rectangle(feature_img, brect.tl(), brect.br(), cv::Scalar(100, 100, 200), 1, CV_AA);
}
void RouteMaker::display()
{
    cv::imshow("Before", src);
    cv::imshow("Thin&Feature", feature_img);
    cv::imshow("Line", line_img);
    cv::imshow("Route", route_img);
    cv::waitKey(1000);
}
void RouteMaker::boundingRect()
{
    PointArray tmp;
    for(int i=0; i<routes.size(); i++){
	tmp.insert(tmp.end(), routes[i].begin(), routes[i].end());
    }
    brect = cv::boundingRect(tmp);
}
std::vector<RouteMaker::PointfArray> RouteMaker::getRealRoutes()
{
    return real_routes;
}
