#ifndef __INCLUDED_ROUTE_MAKER_HPP__
#define __INCLUDED_ROUTE_MAKER_HPP__

#include <iostream>
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <random>

class RouteMaker
{
public:
    typedef std::vector<cv::Point> PointArray;
    typedef std::vector<cv::Point2f> PointfArray;
private:
    class Line
    {
    public:
	int start_id, end_id;
	unsigned char start_fe, end_fe;
	cv::Point2f start_vec, end_vec;
	PointArray line;
    };
    static const cv::Point array4[4];
    static const cv::Point array8[8];
    int paper_w;
    int paper_h;
    double interval;
    cv::Mat src;
    cv::Mat src_inv;
    cv::Mat thin;
    cv::Mat features;    
    cv::Mat feature_img,line_img,route_img;
    std::vector<Line> lines;
    std::vector<PointArray> routes;
    std::vector<PointfArray> real_routes;
    cv::Rect brect;
    double image_real_scale;
public:
    RouteMaker(cv::Mat data, int threshold, int w=148, int h=210, double inter=10.0);
    static cv::Vec3b HSV2RGB(float h, float s, float v);
    static void encode(cv::Mat data, int i, int j, int a[]);
    static int connect(int a[]);
    void thinning();
    void featureExtract();
    void makeLine();
    void makeRoute();
    void makeRealRoute();
    void thinOut();
    void makeImage();
    void display();
    void boundingRect();
    std::vector<PointfArray> getRealRoutes();
};

#endif
