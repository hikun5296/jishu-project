#ifndef __INCLUDED_IMAGE2PLOTTER_HPP__
#define __INCLUDED_IMAGE2PLOTTER_HPP__

#include "route_maker.hpp"
#include <vector>
#include <opencv2/opencv.hpp>
class StepperData
{
public:
    int cwccw;
    int pulse_num;
    long delay; //us
};
class PlotterRoute
{
public:
    int pen_down;
    StepperData x,y;
};
class ImageToPlotter
{
private:
    std::vector<PlotterRoute> plotter_routes;
    double max_vel;
    double max_vel2;
    double feed_speed;
    int step;
public:
    ImageToPlotter(double max_vel_, double max_vel2_, double feed_speed_=1.0, int step_=200);
    void makePlotterRoute(std::vector<RouteMaker::PointfArray> routes);
    std::vector<PlotterRoute> getPlotterRoutes();
};

#endif
