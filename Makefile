CC 		= g++
COMMON 		= .
RM 	= rm -f
TARGET 	= main
OBJS 	= route_maker.o image2plotter.o device.o main.o
CFLAGS  = -O3 `pkg-config --cflags opencv` -std=c++0x
LDFLAGS = 
LDLIBS  = `pkg-config --libs opencv` -lboost_thread

.cpp.o:
	${CC} -c ${CFLAGS} $<
${TARGET}: ${OBJS}
	${CC} -o ${TARGET} ${OBJS} ${LDLIBS} ${LDFLAGS} -fopenmp
clean:
	${RM} ${TARGET} *.o *~
