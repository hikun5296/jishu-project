#ifndef __INCLUDED_DEVICE_HPP__
#define __INCLUDED_DEVICE_HPP__

#include <vector>
#include <fstream>
#include <semaphore.h>
#include <time.h>
#include <sched.h>
#include <pthread.h>
#include "image2plotter.hpp"

class Device
{
protected:
    struct timespec ts;
    struct sched_param sp;
    std::vector<std::ofstream* > ofs;
    std::vector<std::ifstream* > ifs;
    std::vector<int> out_pins;
    std::vector<int> in_pins;
    void openIOPin(int pin, const char* direction);
    void closeIOPin(void);
    void setIOPin(int index, int value);
    void sleepUntil(long delay);
    void setTime(void);
    void setPriority(int priority);
public:
    static void panic(const char* message);
    sem_t sem;
    sem_t rt_sem;
    int cmd, status;
    Device(void);
    virtual ~Device(void);
    virtual void thread(void)=0;
};
class Stepper : public Device
{
public:
    Stepper(int cwcww, int clk);
    ~Stepper(void);
    void thread(void);
    
    StepperData data;
};
class Solenoid : public Device
{
private:
    int old_down;
public:
    Solenoid(int pin);
    ~Solenoid(void);
    void thread(void);
    int down;
};

#endif
