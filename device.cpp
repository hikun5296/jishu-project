#include "device.hpp"
#include <sstream>
#include <iostream>
#include <iomanip>

void Device::panic(const char *message)
{
    std::cerr << "Fatal error: " << message << std::endl;
    exit(1);
}
Device::Device(void)
{
    sem_init(&sem, 0, 0);
    sem_init(&rt_sem, 0, 0);
    cmd = 0;
    status = 0;
}
Device::~Device(void)
{
    closeIOPin();
}
void Device::sleepUntil(long delay)
{
    ts.tv_nsec += delay*1000;
    if(ts.tv_nsec > 1000*1000*1000){
	ts.tv_nsec -= 1000*1000*1000;
	ts.tv_sec++;
    }
    clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &ts,  NULL);
}
void Device::setTime(void)
{
    clock_gettime(CLOCK_MONOTONIC, &ts);
}
void Device::setPriority(int priority)
{
    sp.sched_priority = priority;
    if(pthread_setschedparam(pthread_self(), SCHED_RR, &sp)){
	std::cerr << "WARNING: Failed to set thread to realtime priority" << std::endl;
    }
}
void Device::openIOPin(int pin, const char* direction)
{
    #ifdef GPIO
    std::ofstream ofs_;
    ofs_.open("/sys/class/gpio/export");
    ofs_ << pin;
    ofs_.close();
    std::stringstream ss1,ss2;
    ss1 << "/sys/class/gpio/gpio" << pin << "/direction";
    ofs_.open(ss1.str().c_str());
    if(ofs_.fail()){
	panic("Could not open gpio file");
    }
    ofs_ << direction;
    ofs_.close();
    ss2 << "/sys/class/gpio/gpio" << pin << "/value";
    #else
    std::stringstream ss2;
    ss2 << "log_device" << pin << ".dat";
    #endif
    switch(direction[0]){
    case 'o':
	ofs.push_back(new std::ofstream(ss2.str().c_str(), std::ios::app));
	out_pins.push_back(pin);
	break;
    case 'i':
	ifs.push_back(new std::ifstream(ss2.str().c_str()));
	in_pins.push_back(pin);
	break;
    default:
	break;
    }
}
void Device::closeIOPin(void)
{
    for(int i=0; i<ofs.size(); i++){
	delete ofs[i];
    }
    for(int i=0; i<ifs.size(); i++){
	delete ifs[i];
    }
    #ifdef GPIO
    for(int i=0; i<out_pins.size(); i++){
	std::stringstream ss;
	std::ofstream ofs_;
	ss << "/sys/class/gpio/gpio" << out_pins[i] << "/direction";
	ofs_.open(ss.str().c_str());
	if(ofs_.fail()){
	    panic("Could not open gpio file");
	}
	ofs_ << "in";
	ofs_.close();
	ofs_.open("/sys/class/gpio/unexport");
	ofs_ << out_pins[i];
	ofs_.close();
    }
    for(int i=0; i<in_pins.size(); i++){
	std::stringstream ss;
	std::ofstream ofs_;
	ss << "/sys/class/gpio/gpio" << in_pins[i] << "/direction";
	ofs_.open(ss.str().c_str());
	if(ofs_.fail()){
	    panic("Could not open gpio file");
	}
	ofs_ << "in";
	ofs_.close();
	ofs_.open("/sys/class/gpio/unexport");
	ofs_ << in_pins[i];
	ofs_.close();
    }
    #endif
}
void Device::setIOPin(int index, int value)
{
    #ifdef GPIO
    *(ofs[index]) << value;
    ofs[index]->flush();
    #else
    struct timespec tmp;
    clock_gettime(CLOCK_MONOTONIC, &tmp);
    *(ofs[index]) << tmp.tv_sec<<"."<<std::setfill('0')<<std::setw(9)<<std::right<<tmp.tv_nsec << " " << value << std::endl;
    #endif
}
Stepper::Stepper(int cwccw, int clk) : Device()
{
    openIOPin(cwccw, "out");
    openIOPin(clk, "out");
}
Stepper::~Stepper(){}
void Stepper::thread(void)
{
    setIOPin(0,0);
    setIOPin(1,0);
    setPriority(30);
    while(1){
	sem_wait(&sem);
	switch(cmd){
	case 1:
	    setIOPin(0, data.cwccw);
	    setTime();
	    status = 1;
	    for(int i=0; i<data.pulse_num; i++){
		if(!sem_trywait(&sem)){
		    if(cmd == 0){
			status = 0;
			break;
		    }else if(cmd == -1){
			status = -1;	
			setIOPin(0,0);
			setIOPin(1,0);
			return;
		    }
		}
		setIOPin(1, 1);
		sleepUntil(data.delay);
		setIOPin(1, 0);
		sleepUntil(data.delay);
	    }
	    setIOPin(0,0);
	    setIOPin(1,0);
	    sem_post(&rt_sem);
	    break;
	case -1:
	    status = -1;	
	    setIOPin(0,0);
	    setIOPin(1,0);
	    return;
	    break;
	case 0:
	    sem_post(&rt_sem);
	    break;
	}
	
    }
}
Solenoid::Solenoid(int pin) : Device()
{
    openIOPin(pin, "out");
    old_down = 0;
}
Solenoid::~Solenoid(void){}
void Solenoid::thread(void)
{
    setPriority(25);
    setIOPin(0, 0);
    while(1){
	sem_wait(&sem);
	switch(cmd){
	case 1:
	    if(old_down != down){
		usleep(1000*1000);
		setIOPin(0,down);
		status = 1;
		usleep(1000*1000);
	    }
	    old_down = down;
	    status = 1;
	    sem_post(&rt_sem);
	    break;
	case -1:
	    setIOPin(0,0);
	    status = -1;
	    return;
	case 0:
	    setIOPin(0,0);
	    status = 0;
	    sem_post(&rt_sem);
	    break;
	}
    }
}
